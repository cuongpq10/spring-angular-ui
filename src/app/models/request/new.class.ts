import { Identifier } from 'typescript';
import { NewsCategory } from './category-new.class';

export class News{
    id: number;
    title : string;
    description: string;
    image: string;
    content: string;
    category: NewsCategory;
    timeUpdate: string;

}