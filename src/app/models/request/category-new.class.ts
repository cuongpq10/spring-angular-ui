import { News } from '../response/news.class';

export class NewsCategory {
    id: number;
    title: string;
    description: string;
    image: string;
    content: string;
    news: any;
    timeUpdate: string;
}