import { Component, OnInit, AfterContentChecked, ViewChild } from '@angular/core';

import { AdminServiceService } from './service/admin-service.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterContentChecked {
  title = 'vtanphatUI';
  showLoading = false;

  private readonly notifier: NotifierService
  constructor(
    private adminService: AdminServiceService,
    notifier: NotifierService
  ) {
    this.notifier = notifier;
  }
  ngOnInit() {
    this.showLoading = this.adminService.isLoading;
  }
  ngAfterContentChecked() {
    this.showLoading = this.adminService.isLoading;

  }
  showNotification(type: string, notifi: string) {
    this.adminService.showNotification(type, notifi);
  }

}
