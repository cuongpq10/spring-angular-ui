import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../component/confirm-dialog/confirm-dialog.component';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDialogService {

  constructor(
    private dialog : MatDialog
  ) { }

  openConfirmDialog(msg : string){
    return this.dialog.open(ConfirmDialogComponent,{
      width: '390px',
      disableClose: true,
      data: {
        message: msg 
      }
    })
  }
}
