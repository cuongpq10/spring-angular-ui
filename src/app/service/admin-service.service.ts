import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http'
import { UserAdmin } from '../models/response/user-admin.class';
import { LoginRequest } from '../models/request/login-request.class';
import { BaseResponse } from '../models/response/base-response.class';
import { Observable, throwError } from 'rxjs';
import { LoginResponse } from '../models/response/login-response.class';
import { NotifierService } from 'angular-notifier';
import { TokenRequest } from '../models/request/token-request.class';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {
  isLoading = false;
  private readonly notifier: NotifierService
  private userAdmin: UserAdmin;
  private api: string;

  constructor(
    private http: HttpClient,
    notifier: NotifierService,
    private router : Router
  ) {  
    this.notifier = notifier;

  }
  
  /**
   * doLogin
   */
  checkLogin(): Observable<BaseResponse>{
    this.api = "/api/expired";
    let request = new TokenRequest();
    request.token = this.getToken();
    return this.http.post<BaseResponse>(this.api, request);
  }

  public doLogin(request: LoginRequest): Observable<BaseResponse> {
    this.api = "/api/login";
    return this.http.post<BaseResponse>(this.api, request);
  }
  
  public doGetUserInfo(): Observable<BaseResponse> {
    this.api = "/api/profile";
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', this.getToken());
    return this.http.post<BaseResponse>(
      this.api,
      {
        "title": 'added title'
      }, {headers});
  }

  storeToken(token: string) {
    if (token === "") {
      return;
    }
    localStorage.setItem("token", token);
  }

  getToken() {
    return localStorage.getItem("token");
  }

  removeToken() {
    return localStorage.removeItem("token");
  }

  set _userInfo(user: UserAdmin) {
    this.userAdmin = user;
  }

  get _userInfo() {
    return this.userAdmin;
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  doLogout(){
    localStorage.removeItem("token");
    localStorage.removeItem("userinfo");
    this.router.navigate(["admin/admin-login"]);
  }

  public showNotification( type: string, message: string ): void {
		this.notifier.notify( type, message );
  }

  public hideOldestNotification(): void {
		this.notifier.hideOldest();
  }

  public hideNewestNotification(): void {
		this.notifier.hideNewest();
  }

  public hideAllNotifications(): void {
		this.notifier.hideAll();
  }

  public showSpecificNotification( type: string, message: string, id: string ): void {
		this.notifier.show( {
			id,
			message,
			type
		} );
  }

  public hideSpecificNotification( id: string ): void {
		this.notifier.hide( id );
	}
}
