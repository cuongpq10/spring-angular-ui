import  {Routes} from '@angular/router'

import { AdminLoginComponent } from '../component/admin/admin-login/admin-login.component';
import { DashboardComponent } from '../component/admin/dashboard/dashboard.component';
import { AdminComponent } from '../component/admin/admin.component';
import { ClientComponent } from '../component/client/client.component';
import { HomeComponent } from '../component/client/home/home.component';
import { AdminHeaderComponent } from '../component/admin/admin-header/admin-header.component';
import { AdminFooterComponent } from '../component/admin/admin-footer/admin-footer.component';
import { AdminNewsComponent } from '../component/admin/admin-news/admin-news.component';
import { InitNewComponent } from '../component/admin/admin-news/init-new/init-new.component';
import { CategoryInitNewComponent } from '../component/admin/admin-news/category-init-new/category-init-new.component';
import { CategoryNewComponent } from '../component/admin/admin-news/category-new/category-new.component';
import { PageNotFoundComponent } from '../component/page-not-found/page-not-found.component';
export const appRoutes : Routes=[
    {
        path:'',
        redirectTo:'admin',
        pathMatch: 'full'
        // children:[
        //     {
        //         path: 'home',
        //         component: HomeComponent
        //     }
        // ]
    },{
        path:'admin',
        component: AdminComponent,
        children:[
            {
                path : 'admin-login',
                component: AdminLoginComponent
            },{
                path : 'dashboard',
                component: DashboardComponent
            },{
                path : 'admin-news',
                component: AdminNewsComponent
                
            },{
                path: 'init-new',
                component : InitNewComponent,
                children: [
                    {
                        path: ':id',
                        component : InitNewComponent
                    }
                ]
            },{
                path: 'category-new',
                component : CategoryNewComponent
            },{
                path: 'category-init-new',
                component : CategoryInitNewComponent,
                children: [
                    {
                        path: ':id',
                        component : CategoryInitNewComponent
                    }
                ]
            }
        ]
    },{
        path: "page-not-found",
        component:PageNotFoundComponent
    }
]