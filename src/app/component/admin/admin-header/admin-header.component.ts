import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { Router } from '@angular/router';
import { AdminServiceService } from 'src/app/service/admin-service.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit, AfterContentChecked {

  constructor(
    private router: Router,
    private adminService : AdminServiceService
  ) { }
  isVisibleLogOut: boolean = true;
  doSetBackgroundMenu = 1;
  ngOnInit(): void {
  }
  onVisibleLogout() {
    this.isVisibleLogOut = !this.isVisibleLogOut;
  }
  doLogout() {
    this.adminService.doLogout();
  }
  onSetBackground(val) {
    this.doSetBackgroundMenu = val;
  }
  ngAfterContentChecked() {
    if (this.router.url === "/admin/dashboard") {
      this.doSetBackgroundMenu = 1;
    }
    if (this.router.url === "/admin/admin-news") {
      this.doSetBackgroundMenu = 2;
    }
    if (this.router.url === "/admin/category-new") {
      this.doSetBackgroundMenu = 3;
    }

  }

}
