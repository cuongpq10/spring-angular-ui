import { Component, OnInit, AfterContentInit, AfterViewInit } from '@angular/core';
import { News } from 'src/app/models/response/news.class';
import { NewsService } from '../service/news.service';
import { BaseResponse } from 'src/app/models/response/base-response.class';
import { Router } from '@angular/router';
import { AdminServiceService } from 'src/app/service/admin-service.service';
import { ConfirmDialogService } from 'src/app/service/confirm-dialog.service';
import { data } from 'jquery';

@Component({
  selector: 'app-admin-news',
  templateUrl: './admin-news.component.html',
  styleUrls: ['./admin-news.component.css']
})
export class AdminNewsComponent implements OnInit, AfterViewInit {
  listNews: Array<News>;
  constructor(
    private newsService: NewsService,
    private router: Router,
    private adminService: AdminServiceService,
    private dialogService : ConfirmDialogService
  ) { }

  ngOnInit(): void {

  }
  ngAfterViewInit() {
    this.adminService.isLoading = true;
    this.newsService.doGetAllNews().subscribe(
      (baseResponse: BaseResponse) => {
        this.onSuccessGetListNews(baseResponse);
      }, (throwable) => {
        this.onErrorGetListNews(throwable);
      }
    );
  }
  onSuccessGetListNews(baseResponse: BaseResponse) {
    this.adminService.isLoading = false;
    if (baseResponse.code == 0) {
      if (!baseResponse.data || baseResponse.data.length === 0) {
        this.adminService.showNotification("success", "Chưa có bản tin nào");
        return;
      }
      this.listNews = baseResponse.data;
      this.newsService._listNews = baseResponse.data;
    } else {
      this.adminService.showNotification("error", baseResponse.message);

    }
  }
  onErrorGetListNews(throwable: any) {
    this.adminService.isLoading = false;
    this.router.navigate(['page-not-found']);
  }
  doOpenAddNewScreen() {
    this.router.navigate(["admin/init-new"]);
  }

  doGoUpdateNew(id: number){
    let api = "/admin/init-new";
    this.router.navigate([api,{id:id}]);
  }

  public doDeleteNew(id: number){
    this.dialogService.openConfirmDialog("Bạn chắc chắn xóa bản tin này ?")
    .afterClosed()
    .subscribe(
      (res)=>{
        if(res){
          this.adminService.isLoading= true;
          this.newsService.doDeleteNew(id).subscribe(
            (baseResponse: BaseResponse)=>{
              this.onSuccessDeleteNew(baseResponse, id);
            },error=>{
              this.onErrorDeleteNew(error);
            }
          )
        }
      }
    )
  }
  getPosition(ids: number): number {
    let position: number;
    this.listNews.forEach(
      (item, pos) => {
        if (item.id === ids) {
          position = pos;
        }
      }
    )
    return position;
  }
  public onSuccessDeleteNew(baseResponse: BaseResponse,id : number){
    this.adminService.isLoading= false;
    if(baseResponse.code===0){
      this.listNews.splice(this.getPosition(id),1);
      this.adminService.showNotification("info", "Xóa thành công!");
    }else{
      this.adminService.showNotification("info", baseResponse.message);
    }
  }
  public onErrorDeleteNew(error: any){
    this.adminService.isLoading = false;
    this.dialogService.openConfirmDialog("Có lỗi xảy ra, trở về trang đăng nhập !")
    .afterClosed()
    .subscribe(
      (res)=>{
        if(res){
          this.adminService.doLogout();
        }
      }
    )
  }
}
