import { Component, OnInit, AfterContentInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { NewsCategory } from 'src/app/models/request/category-new.class';
import { NewsService } from '../../service/news.service';
import { AdminServiceService } from 'src/app/service/admin-service.service';
import { BaseResponse } from 'src/app/models/response/base-response.class';
import { ConfirmDialogService } from 'src/app/service/confirm-dialog.service';

@Component({
  selector: 'app-category-new',
  templateUrl: './category-new.component.html',
  styleUrls: ['./category-new.component.css']
})
export class CategoryNewComponent implements OnInit, AfterViewInit {
  listCategory: NewsCategory[] = [];
  
  constructor(
    private router: Router,
    private newsService: NewsService,
    private adminService: AdminServiceService,
    private dialogService : ConfirmDialogService
  ) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(){
    this.adminService.isLoading = true;
    this.newsService.doGetAllNewsCategory().subscribe(
      (baseResponse: BaseResponse) => {
        this.onSuccessGetListCategory(baseResponse)
      }, (throwable) => {
        this.onErrorGetListCategory(throwable);
      }
    );
  }
  doGoUpdateCategory(id){
    let api = "/admin/category-init-new";
    this.router.navigate([api,{id:id}]);
  }
  doOpenAddCategoryScreen() {
    this.router.navigate(['admin/category-init-new']);
  }
  doGoUpdateCategoryScreen(id){
    let api = "/admin/category-init-new/${id}";
    this.router.navigate([api])
  }

  onSuccessGetListCategory(baseResponse: BaseResponse) {
    if (baseResponse.code === 0) {
      this.adminService.isLoading = false;
      this.listCategory = baseResponse.data;
      this.newsService._listCategory = baseResponse.data;
    } else {
      this.adminService.isLoading = false;
      this.adminService.showNotification("info", baseResponse.message);
    }
  }
  onErrorGetListCategory(throwble: any) {
    this.adminService.isLoading = false;
    this.adminService.showNotification("info", "Không kết nối được với Server");
  }
  doDeleteCategory(id) {
    this.dialogService.openConfirmDialog("Bạn chắc chắn xóa chứ ? ")
    .afterClosed()
    .subscribe(
      res=>{
        if(res){
          this.adminService.isLoading = true;
          this.newsService.doDeleteCategory(id).subscribe(
            (baseResponse: BaseResponse) => {
              this.onSuccessDeleteCategory(baseResponse, id);
            }, (throwable) => {
              this.onErrorDeleteCategory(throwable);
            }
          )
        }
      }
    )
  }
  getPosition(ids: number): number {
    let position: number;
    this.listCategory.forEach(
      (item, pos) => {
        if (item.id === ids) {
          position = pos;
        }
      }
    )
    return position;
  }
  onSuccessDeleteCategory(baseResponse: BaseResponse, id: number) {
    this.adminService.isLoading=false;
    if (baseResponse.code === 0) {
      this.listCategory.splice(this.getPosition(id),1);
      this.adminService.showNotification("info", "Xóa thành công!");
    } else {
      this.adminService.showNotification("info", baseResponse.message);
    }
  }
  onErrorDeleteCategory(throwable: any) {
    this.adminService.isLoading = false;
    this.dialogService.openConfirmDialog("Có lỗi xảy ra, trở về trang đăng nhập !")
    .afterClosed()
    .subscribe(
      (res)=>{
        if(res){
          this.adminService.doLogout();
        }
      }
    )
  }
}
