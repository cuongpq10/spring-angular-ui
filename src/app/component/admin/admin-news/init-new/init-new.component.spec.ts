import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitNewComponent } from './init-new.component';

describe('InitNewComponent', () => {
  let component: InitNewComponent;
  let fixture: ComponentFixture<InitNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
