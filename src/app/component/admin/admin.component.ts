import { Component, OnInit, AfterContentChecked, AfterViewInit } from '@angular/core';
import { AdminServiceService } from 'src/app/service/admin-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, AfterContentChecked,AfterViewInit {
  isShowMenu: boolean = true;
  
  constructor(
    private adminService: AdminServiceService,
    private router: Router
  ) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(){
    if(this.adminService.getToken()&& localStorage.getItem("userinfo")){
      this.doCheckLastLogin();
    }else{
      this.router.navigate(["admin/admin-login"]);
    }
  }
  doCheckLastLogin(){
    this.adminService.checkLogin()
      .subscribe(data => {
        if(data.data === false){
          this.adminService.showNotification("error","Có lỗi phiên đăng nhập lần trước! Vui Lòng đăng nhập lại");
          this.adminService.doLogout();
        }else{
          this.router.navigate(["admin/dashboard"]);
        }
      },error=>{
        this.adminService.showNotification("error","Có lỗi phiên đăng nhập lần trước! Vui Lòng đăng nhập lại");
        this.adminService.doLogout();
      });
  }

  ngAfterContentChecked() {
    if (this.router.url === "/admin/admin-login") {
      this.isShowMenu = false;
    } else {
      this.isShowMenu = true;
    }
  }

}
