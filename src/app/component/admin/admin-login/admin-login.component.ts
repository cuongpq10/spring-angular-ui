import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { AdminServiceService } from 'src/app/service/admin-service.service';
import { Router } from '@angular/router';
import { LoginRequest } from '../../../models/request/login-request.class';
import { LoginResponse } from 'src/app/models/response/login-response.class';
import { BaseResponse } from 'src/app/models/response/base-response.class';
import { UserAdmin } from 'src/app/models/response/user-admin.class';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  formdata;
  loginStatus: number;

  constructor(
    private adminService: AdminServiceService,
    private router: Router
  ) { }
  

  ngOnInit(): void {
    this.formdata = new FormGroup({
      uname: new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])),
      passwd: new FormControl("", this.passwordValidator)
    })
  }

  passwordValidator(formControl) {
    if (formControl.value.length < 5) {
      return { "passwd": true }
    }
  }

  onClickSubmit(data) {
    this.adminService.isLoading= true;
    let request = new LoginRequest();
    request.username = data.uname;
    request.password = data.passwd;
    this.adminService.doLogin(request).subscribe((baseResponse: BaseResponse) => {
      this.onSuccessLogin(baseResponse);
    }, (throwable) => {
      this.onErrorLogin(throwable);
    });
  }

  onSuccessLogin(baseResponse: BaseResponse) {
    if (baseResponse.code === 0) {
      this.adminService.showNotification("success","Đăng nhập thành công");
      this.adminService.storeToken("Bearer "+baseResponse.data.token);
      this.adminService.doGetUserInfo().subscribe(
        (baseResponse: BaseResponse) => {
          this.onSuccessGetUserInfo(baseResponse)
        }, (throwable) => {
          this.onErrorGetUserInfo(throwable);
        }
      );
    } else {
      this.adminService.isLoading= false;
      this.loginStatus = baseResponse.code;
    }
  }

  onErrorLogin(thorwable?: any) {
    this.adminService.isLoading= false;
    this.adminService.showNotification("danger","Đăng nhập thất bại");
  }

  onSuccessGetUserInfo(baseResponse: BaseResponse) {
    if (baseResponse.code == 0) {
      localStorage.setItem("userinfo", baseResponse.data);
      setTimeout(()=>{ 
        this.adminService.isLoading =false;
        this.router.navigate(['admin/dashboard']);
        }, 2000)
      
    } else {
      this.adminService.isLoading= false;
      this.adminService.showNotification("danger", baseResponse.message);
    }
  }

  onErrorGetUserInfo(throwable?: any) {
    this.adminService.isLoading= false;
    this.adminService.showNotification("danger","Lấy dữ liệu thất bại");
    setTimeout(()=>{ this.router.navigate(['page-not-found']); }, 4000)
    
  }
}
